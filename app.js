const express=require('express');
const app=express();

const mongoose=require('mongoose')
mongoose.connect('mongodb://localhost:27017/callforjobs')
.then(()=>console.log(`Mongo DB connected`))
.catch(()=>console.log(`Mongo DB not connected`));

//Express middleware
app.use(express.json());

const userRouter= require("./routes/user");
const queryRouter= require('./routes/querie');

//Router middleware
app.use('/users',userRouter);
app.use('/queries',queryRouter);

const port=process.env.PORT||3000;
app.listen(port,()=>console.log(`Server started on ${port}`))