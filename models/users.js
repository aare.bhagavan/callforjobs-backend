const mongoose=require('mongoose');
//Creating contactSchema
const userSchema=new mongoose.Schema({
    name:{
        type:String,
        require:[true,"name is required"]

    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    number:{
        type:Number,
        required:[true,"mobile number is required"]
    },
    education:{
        type:String
    },
    experience:{
        type:String,
        default:0

       
    },
    previous_company:{
        type:String,
        default:""
    },
    previous_role:{
        type:String,
        default:""
    }
})
//Fitting queriesModel
const usersModel=mongoose.model('users',userSchema);
module.exports=usersModel;