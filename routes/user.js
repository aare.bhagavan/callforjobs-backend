const express=require('express');
const router=express.Router();

const userModel=require('../models/users');

router.post('/',async(req,res)=>{
    try{
        let userData={
            name:req.body.name,
            email:req.body.email,
            number:req.body.number,
            education:req.body.education,
            experience:req.body.experience
        }
        let exp = req.body.experience;
        if(exp > 0){
            userData["previous_company"] = previous_company;
            userData["previous_role"]=previous_role
        }
        let userCreate=await userModel.create(userData);
        return res.status(201).json({success:true,data:userCreate})
    }
    catch(err){
        return res.status(500).json({success:false,msg:err.msg})
    }
})

module.exports=router;