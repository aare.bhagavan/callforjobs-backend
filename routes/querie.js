const express=require('express');
const router=express.Router();

const queriesModel=require('../models/queries');

router.post('/',async(req,res)=>{
    try{
        let queryData={
            name:req.body.name,
            email:req.body.email,
            number:req.body.number,
            address:req.body.address,
            message:req.body.message
            
    
        }
        let queryCreate=await queriesModel.create(queryData);
        return res.status(201).json({success:true,data:queryCreate})
    }
    catch(err){
        return res.status(500).json({success:false,msg:err.msg})
    }
})

module.exports = router;